use std::{net::SocketAddr, time::SystemTime};

use axum::{
    body::{boxed, Body, BoxBody, HttpBody},
    extract::State,
    http::{request, HeaderName},
    response::Response,
    routing::{get, post},
    Json,
};
use reqwest::{header, Method};
use serde::Serialize;
use time::OffsetDateTime;

const INFLUX_BUCKET: &str = "data";
const INFLUX_ORG: &str = "pluth";

#[derive(Debug, Serialize)]
struct Timestamp {
    timestamp: i64,
    offset_hms: (i8, i8, i8),
}

async fn get_timestamp() -> Json<Timestamp> {
    let time = OffsetDateTime::now_local().unwrap();

    let timestamp = time.unix_timestamp();
    let offset_hms = time.offset().as_hms();

    Json(Timestamp {
        timestamp,
        offset_hms,
    })
}

#[derive(Clone)]
struct MyState {
    client: reqwest::Client,
}

async fn post_influx(State(state): State<MyState>, body: String) -> Response {
    let mut lines = body.lines();
    let auth = lines.next().unwrap();

    let response = state
        .client
        .request(
            Method::POST,
            format!("http://localhost:8086/api/v2/write?bucket={INFLUX_BUCKET}&org={INFLUX_ORG}"),
        )
        .header(header::AUTHORIZATION, format!("TOKEN {auth}"))
        .body(lines.collect::<String>())
        .send()
        .await
        .unwrap();

    let axum_res = Response::builder().status(response.status());
    axum_res
        .body(boxed(
            Body::wrap_stream(response.bytes_stream()).map_err(axum::Error::new),
        ))
        .unwrap()
}

#[tokio::main(flavor = "current_thread")]
async fn main() -> color_eyre::Result<()> {
    let router = axum::Router::new()
        .route("/", get(get_timestamp))
        .route("/post", post(post_influx))
        .with_state(MyState {
            client: reqwest::Client::new(),
        });

    let addr: SocketAddr = "0.0.0.0:8060".parse().unwrap();
    axum::Server::bind(&addr)
        .serve(router.into_make_service())
        .await?;

    Ok(())
}
